var gulp = require('gulp');
var livereload = require('gulp-livereload');
var nodemon = require('gulp-nodemon');
var mocha = require('gulp-mocha');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var uglifycss = require('gulp-uglifycss');


gulp.task('livereload', function() {

  //開啟livereload server，讓前端的livereload套件可以與之連接
  livereload.listen();

  //livereload server會去監控下列檔案，當檔案被更動則馬上刷新畫面
  gulp.watch(['*.js', 'controllers/*.js', 'views/*.ejs','/public/*.css'], function(file) {
    console.log("change " + file.path);
    livereload.changed(file.path);
  });
});


gulp.task('nodemon', function() {

  var stream = nodemon({
    script: 'bin/www',
    ext: 'js'
    // ignore: ['ignored.js'],
    // tasks: ['lint']
  });

  stream
    .on('restart', function() {
      console.log('restarted!');
    })
    .on('crash', function() {
      console.error('Application has crashed!\n');
      stream.emit('restart', 5); // restart the server in 5 seconds 
    });
});


gulp.task('mocha', function() {

  gulp.src('test/test.js', {read: false}).pipe(mocha()).on('error',console.log);;

  gulp.watch(['*.js', 'controllers/*.js', 'db/*.js', 'test/*.js'], function(file) {
    console.log("change " + file.path);
    return gulp.src(['test/test.js'],{read:false})
    .pipe(mocha())
    .on('error',console.log);
  });
});


var vendorjs = [
  'public/library/jquery-2.1.4/jquery-2.1.4.min.js',
  'public/library/jquery-2.1.4/jquery-ui.js',
  'public/library/boostrap/js/bootstrap.js',
  'public/library/flatpickr/flatpickr.js',
  'public/library/bootstrap-table/bootstrap-table.min.js',
  'public/library/bootstrap-table/locale/bootstrap-table-zh-TW.min.js',
  'public/library/chardin.js-master/chardinjs.js',
  'public/library/sweet/es6-promise.min.js',
  'public/library/sweet/sweetalert2.min.js',
  'public/library/animation/animsition.min.js',
  'public/library/jQuery-slidepanel/jquery.slidepanel.js',
  'public/library/jquery-blockui/jquery.blockUI.min.js',
  'public/library/jqueryui-touch-punch/jquery.ui.touch-punch.min.js',
  'public/library/bootstrap-wysiwyg/ace.js',
  'public/library/bootstrap-wysiwyg/bootstrap-wysiwyg.js',
  'public/library/bootstrap-wysiwyg/ace-elements.js',
  'public/library/jQuery-Tags-Input-master/jquery.tagsinput.js'
];

  

var appjs = [
  'public/js/fileinput.js',
  'public/js/jquery.ns-autogrow.js',
  'public/js/jquery.tree.js',
  'public/js/lsheet.js',
  'public/js/menuhover.js',
  'public/js/scroll.js',
  'public/js/statuscode-html.js',
  'public/js/tablejs.js',
  'public/js/utility.js',
  'public/js/jquery.ns-autogrow.js'
];

var stylecss = [
  'public/css/animate.css',
  'public/css/fileinput.css',
  'public/css/loaders.css',
  'public/css/print.css',
  'public/css/print.css_bak.css',
  'public/css/process.css',
  'public/css/robot-dialog.css',
  'public/css/scroll.css',
  'public/css/ls-style.css'
]; 

var vendorcss=[
'public/library/jquery-2.1.4/jquery-ui.css',
'public/library/boostrap/css/bootstrap.css',
'public/library/sweet/sweetalert2.min.css',
'public/library/chardin.js-master/chardinjs.css',
'public/library/flatpickr/flatpickr.css',
'public/library/font-awesome/css/font-awesome.css',
'public/library/font-awesome/css/font-awesome-animation.css',
'public/library/jQuery-slidepanel/jquery.slidepanel.css',
'public/library/animation/animsition.css',
'public/library/Hover-master/hover.css',
'public/library/bootstrap-wysiwyg/ace.css',
'public/library/hint.css-2.4.1/hint.css',
'public/library/jQuery-Tags-Input-master/jquery.tagsinput.css'
];


gulp.task('uglify-app-js', function() {
 return gulp.src(appjs)
    .pipe(concat('app.min.js'))
    .pipe(uglify ())
    .pipe(gulp.dest('./public/js'))
    .on('error',console.log);
});    

gulp.task('uglify-vendor-js', function() {
 return gulp.src(vendorjs)
    .pipe(concat('vendor.min.js'))
    .pipe(uglify ())
    .pipe(gulp.dest('./public/js'))
    .on('error',console.log);
});

gulp.task('uglify-app-css', function() {
 return gulp.src(stylecss)
    .pipe(concat('style.min.css'))
    .pipe(uglifycss ())
    .pipe(gulp.dest('./public/css'))
    .on('error',console.log);
});

gulp.task('uglify-vendor-css', function() {
 return gulp.src(vendorcss)
    .pipe(concat('vendor.min.css'))
    .pipe(uglifycss ())
    .pipe(gulp.dest('./public/css'))
    .on('error',console.log);
});

gulp.task('uglify', ['uglify-app-js','uglify-app-css','uglify-vendor-js','uglify-vendor-css']);


//下gulp指令，將會執行default旗下的所有task
gulp.task('default', ['nodemon', 'livereload']);

//下gulp test執行後，只要修改程式碼，gulp-mocha自動會幫忙測試
gulp.task('test', ['mocha']);