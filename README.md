# [avatar-maker]

This is simple website which can make avatar by inputing text and selecting decoration. 
You can download avatar directly or just post the avatar. The avatar you post will save on server and show below the page.
You can also watch other people's avatar.

## Features

* There is no database required.
* All avatar image will use timestamp as filename, so page can sort avatar list by created time.
